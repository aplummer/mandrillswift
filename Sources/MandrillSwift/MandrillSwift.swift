import Foundation

public protocol MandrillSwiftAPITraits {
    
    var api: API { get }
    
}

public struct MandrillSwiftAPI: MandrillSwiftAPITraits {
    
    public let api: API
    
    public init(_ apiKey: String, timeout: TimeInterval = 15) throws {
        
        guard
            let dc = apiKey.split(separator: "-").last,
            let baseURL = URL(string: "https://mandrillapp.com") else {
                throw GeneralError.unknown
        }
        
        api = API(apiKey: apiKey, baseURL: baseURL, timeout: 15)
        
    }
    
    
}
