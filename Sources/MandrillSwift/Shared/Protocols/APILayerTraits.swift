//
//  APILayerTraits.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 4/2/18.
//

import Foundation

public protocol APILayerTraits {
    
    var apiKey: String { get }
    
    /// The base URL for the mailchimp API
    var baseURL: URL { get }
    
    /// A default timeout for network requests
    var timeout: TimeInterval { get }
    
    func getJSONResponse<T>(
        urlRequest: inout URLRequest,
        callback: ((APIResponse<T>) -> Void)?
    )
    
}

extension APILayerTraits {
    
    public func getJSONResponse<T>(
        urlRequest: inout URLRequest,
        callback: ((APIResponse<T>) -> Void)? = nil) {
        
        let session = URLSession(configuration: .default)
        
        urlRequest.setValue(
            "application/x-www-form-urlencoded",
            forHTTPHeaderField: "Content-Type"
        )
        
        let dataTask = session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            guard error == nil else {
                callback?(.error(.generic))
                return
            }
            
            let decoder = JSONDecoder()
            
            guard let data = data else {
                callback?(.error(.generic))
                return
            }
            
            do {
                
                let response = try decoder.decode(T.self, from: data)
                callback?(.success(response))
                return
                
            } catch let error {
                debugPrint(error)
            }
            
            if let error = try? decoder.decode(APIError.self, from: data) {
                callback?(.error(error))
                return
            }
            
            callback?(.error(.generic))
            
        }
        
        dataTask.resume()
        
    }
    
}
