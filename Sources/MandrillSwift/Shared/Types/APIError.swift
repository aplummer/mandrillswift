//
//  APIError.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 23/1/18.
//

import Foundation

public struct APIError: Error, Codable {
    
    enum APIErrorType: String {
        case
            invalidKey = "Invalid_Key",
            serviceUnavailable = "ServiceUnavailable",
            validationError = "ValidationError",
            generalError = "GeneralError",
            // Fallback
            unknownError = "UnknownError"
    }
    
    let status: String
    let code: Int
    let name: String
    let message: String
    
    var type: APIErrorType {
        return APIErrorType(rawValue: name) ?? .unknownError
    }
    
    static var generic: APIError {
        return APIError(
            status: "error",
            code: -1,
            name: "UnknownError",
            message: "An unknown error occurred"
        )
    }
    
}
