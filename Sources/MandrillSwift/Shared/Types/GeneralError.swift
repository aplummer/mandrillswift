//
//  GeneralError.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 3/2/18.
//

import Foundation

public enum GeneralError: Error {
    case unknown,
        serialize
}
