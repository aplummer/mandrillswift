//
//  APIResponse.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 23/1/18.
//

import Foundation

public enum APIResponse<T: Codable> {
    
    case
        success(T),
        error(APIError)
    
}

public typealias APIResponseCallback<T: Codable> = ((APIResponse<T>) -> Void)?
