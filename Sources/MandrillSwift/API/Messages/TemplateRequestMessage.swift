//
//  TemplateRequestMessage.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 3/2/18.
//

import Foundation

extension TemplateRequest {
    
    public struct Message: Codable {
        
        enum CodingKeys: String, CodingKey {
            case
                html,
                text,
                subject,
                fromEmail = "from_email",
                fromName = "from_name",
                to,
                headers,
                important,
                trackOpens = "track_opens",
                trackClicks = "track_clicks",
                autoText = "auto_text",
                autoHTML = "auto_html",
                inlineCss = "inline_css",
                urlStripQs = "url_strip_qs",
                preserveRecipients = "preserve_recipients",
                viewContentLink = "view_content_link",
                bccAddress = "bcc_address",
                trackingDomain = "tracking_domain",
                signingDomain = "signing_domain",
                returnPathDomain = "return_path_domain",
                merge,
                mergeLanguage = "merge_language",
                globalMergeVars = "global_merge_vars",
                mergeVars = "merge_vars",
                tags,
                subaccount,
                googleAnalyticsCampaign = "google_analytics_campaign",
                metadata,
                recipientMetadata = "recipient_metadata",
                attachments,
                images
            
        }
        
        let html: String?
        let text: String?
        let subject: String
        let fromEmail: String
        let fromName: String?
        let to: [To]
        let headers: [String: String]?
        let important: Bool
        let trackOpens: Bool
        let trackClicks: Bool
        let autoText: Bool
        let autoHTML: Bool
        let inlineCss: Bool
        let urlStripQs: Bool
        let preserveRecipients: Bool
        let viewContentLink: Bool
        let bccAddress: String?
        let trackingDomain: String?
        let signingDomain: String?
        let returnPathDomain: String?
        let merge: Bool?
        let mergeLanguage: MergeLanguage
        let globalMergeVars: [Vars]?
        let mergeVars: [MergeVars]?
        let tags: [String]?
        let subaccount: String?
        let googleAnalyticsCampaign: String?
        let metadata: [String: String]?
        let recipientMetadata: [RecipientMetadata]?
        let attachments: [Attachment]?
        let images: [Attachment]?
        
        public init(
            html: String?,
            text: String?,
            subject: String,
            fromEmail: String,
            fromName: String?,
            to: [To],
            headers: [String: String]?,
            important: Bool,
            trackOpens: Bool,
            trackClicks: Bool,
            autoText: Bool,
            autoHTML: Bool,
            inlineCss: Bool,
            urlStripQs: Bool,
            preserveRecipients: Bool,
            viewContentLink: Bool,
            bccAddress: String?,
            trackingDomain: String?,
            signingDomain: String?,
            returnPathDomain: String?,
            merge: Bool?,
            mergeLanguage: MergeLanguage,
            globalMergeVars: [Vars]?,
            mergeVars: [MergeVars]?,
            tags: [String]?,
            subaccount: String?,
            googleAnalyticsCampaign: String?,
            metadata: [String: String]?,
            recipientMetadata: [RecipientMetadata]?,
            attachments: [Attachment]?,
            images: [Attachment]?
            ) {
            
            self.html = html
            self.text = text
            self.subject = subject
            self.fromEmail = fromEmail
            self.fromName = fromName
            self.to = to
            self.headers = headers
            self.important = important
            self.trackOpens = trackOpens
            self.trackClicks = trackClicks
            self.autoText = autoText
            self.autoHTML = autoHTML
            self.inlineCss = inlineCss
            self.urlStripQs = urlStripQs
            self.preserveRecipients = preserveRecipients
            self.viewContentLink = viewContentLink
            self.bccAddress = bccAddress
            self.trackingDomain = trackingDomain
            self.signingDomain = signingDomain
            self.returnPathDomain = returnPathDomain
            self.merge = merge
            self.mergeLanguage = mergeLanguage
            self.globalMergeVars = globalMergeVars
            self.mergeVars = mergeVars
            self.tags = tags
            self.subaccount = subaccount
            self.googleAnalyticsCampaign = googleAnalyticsCampaign
            self.metadata = metadata
            self.recipientMetadata = recipientMetadata
            self.attachments = attachments
            self.images = images
            
        }
        
        public struct To: Codable {
            
            public enum ToType: String, Codable {
                case
                to,
                cc,
                bcc
            }
            
            let email: String
            let name: String?
            let type: ToType
        
            public init(email: String, name: String?, type: ToType = .to) {
                self.email = email
                self.name = name
                self.type = type
            }
            
        }
        
        public enum MergeLanguage: String, Codable {
            case
                mailchimp,
                handlebars
        }
        
        public struct MergeVars: Codable {
            
            let rcpt: String
            let vars: [Vars]
            
            public init(rcpt: String, vars: [Vars]) {
                self.rcpt = rcpt
                self.vars = vars
            }
            
        }
        
        public struct Vars: Codable {
            let name: String
            let content: String
            
            public init(name: String, content: String) {
                self.name = name
                self.content = content
            }
        }
        
        public struct RecipientMetadata: Codable {
            
            let rcpt: String
            let values: [String: String]
            
            public init(rcpt: String, values: [String: String]) {
                self.rcpt = rcpt
                self.values = values
            }
            
        }
        
        public struct Attachment: Codable {
            
            let type: String
            let name: String
            let content: String
            
            public init(type: String, name: String, content: String) {
                self.type = type
                self.name = name
                self.content = content
            }
            
        }
        
    }

}
