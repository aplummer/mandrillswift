//
//  ListScheduledResponse.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 4/2/18.
//

import Foundation

public struct ListScheduledResponse: Codable {

    let _id: String
    let createdAt: String
    let sendAt: String
    let fromEmail: String
    let to: String
    let subject: String
    
    enum CodingKeys: String, CodingKey {
        case
            _id,
            createdAt = "created_at",
            sendAt = "send_at",
            fromEmail = "from_email",
            to,
            subject
    }
    
}
