//
//  TemplateRequest.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 3/2/18.
//

import Foundation

public struct TemplateRequest: Codable {
    
    public struct TemplateContent: Codable {
        let name: String
        let content: String
        
        public init(name: String, content: String) {
            self.name = name
            self.content = content
        }
        
    }
    
    let key: String
    
    let templateName: String
    
    let templateContent: [TemplateContent]
    
    let message: Message
    
    let async: Bool
    
    let ipPool: String?
    
    let sendAt: String?
    
    enum CodingKeys: String, CodingKey {
        case
            key,
            templateName = "template_name",
            templateContent = "template_content",
            message,
            async,
            ipPool = "ip_pool",
            sendAt = "send_at"
    }
    
    public init(
        key: String,
        templateName: String,
        templateContent: [TemplateContent],
        message: Message,
        async: Bool,
        ipPool: String?,
        sendAt: String?
        ) {
        
        self.key = key
        self.templateName = templateName
        self.templateContent = templateContent
        self.message = message
        self.async = async
        self.ipPool = ipPool
        self.sendAt = sendAt
        
    }
    
}
