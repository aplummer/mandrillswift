//
//  Messages.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 3/2/18.
//

import Foundation

public protocol MessagesMethods {
    
    func sendTemplate(
        templateRequest: TemplateRequest,
        callback: APIResponseCallback<[TemplateResponse]>
    ) throws
    
    func listScheduled(
        apiKey: String,
        to: String?,
        callback: APIResponseCallback<[ListScheduledResponse]>
    ) throws
    
}

extension MessagesMethods where Self: APILayerTraits {
    
    public func sendTemplate(
        templateRequest: TemplateRequest,
        callback: APIResponseCallback<[TemplateResponse]> = nil
    ) throws {
        
        guard let url = URL(string: "/api/1.0/messages/send-template.json", relativeTo: baseURL) else {
            throw GeneralError.unknown
        }
        
        var request = URLRequest(
            url: url,
            cachePolicy: .reloadIgnoringLocalCacheData,
            timeoutInterval: timeout
        )
        
        let encoder = JSONEncoder()
        
        guard let dataRep = try? encoder.encode(templateRequest) else {
            callback?(.error(.generic))
            return
        }
        
        request.httpBody = dataRep
        request.httpMethod = HTTPMethod.post.rawValue
        
        getJSONResponse(urlRequest: &request, callback: callback)
        
    }
    
    public func listScheduled(
        apiKey: String,
        to: String?,
        callback: APIResponseCallback<[ListScheduledResponse]>
        ) throws {
        
        guard let url = URL(string: "/api/1.0/messages/list-scheduled.json", relativeTo: baseURL) else {
            throw GeneralError.unknown
        }
        
        var request = URLRequest(
            url: url,
            cachePolicy: .reloadIgnoringLocalCacheData,
            timeoutInterval: timeout
        )
        
        let encoder = JSONEncoder()
        
        var sendDict = [
            "key": apiKey
        ]
        
        if let _to = to {
            sendDict["to"] = _to
        }
        
        guard let dataRep = try? encoder.encode(sendDict) else {
            callback?(.error(.generic))
            return
        }
        
        request.httpBody = dataRep
        request.httpMethod = HTTPMethod.post.rawValue
        
        getJSONResponse(urlRequest: &request, callback: callback)
        
    }
    
}

public struct Messages: APILayerTraits, MessagesMethods {
    
    public let apiKey: String
    public let timeout: TimeInterval
    public let baseURL: URL
    
}

