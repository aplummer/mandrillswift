//
//  TemplateResponse.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 3/2/18.
//

import Foundation

public struct TemplateResponse: Codable {
    
    let email: String
    let status: String
    let rejectReason: String?
    let _id: String
    
    enum CodingKeys: String, CodingKey {
        
        case
        email,
        status,
        rejectReason = "reject_reason",
        _id
        
    }
    
}
