//
//  API.swift
//  MandrillSwift
//
//  Created by Andrew Plummer on 3/2/18.
//

import Foundation

public struct API: APILayerTraits {
    
    public let apiKey: String
    
    public let baseURL: URL
    
    public let timeout: TimeInterval
    
    public let messages: Messages
    
    init(apiKey: String, baseURL: URL, timeout: TimeInterval) {
        
        messages = Messages(apiKey: apiKey, timeout: timeout, baseURL: baseURL)
        
        self.apiKey = apiKey
        self.baseURL = baseURL
        self.timeout = timeout
        
    }
    
}
