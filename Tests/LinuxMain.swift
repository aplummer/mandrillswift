import XCTest
@testable import MandrillSwiftTests

XCTMain([
    testCase(MandrillSwiftTests.allTests),
])
