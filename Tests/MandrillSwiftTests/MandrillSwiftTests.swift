import XCTest
@testable import MandrillSwift

class MandrillSwiftTests: XCTestCase {
    
    func testListScheduledMessages() {
        
        let expectation = XCTestExpectation(description: "List scheduled messages with mandrill")
        
        let mandrill = try! MandrillSwiftAPI("")
        do {
            try mandrill.api.messages.listScheduled(apiKey: "", to: nil) { response in
                
                switch response {
                    
                case .success(let res):
                    expectation.fulfill()
                    debugPrint(res)
                case .error(let err):
                    debugPrint(err)
                    XCTFail("Unable to list scheduled messages")
                }
                
            }
        } catch let error {
            XCTFail(error.localizedDescription)
        }
        
        wait(for: [expectation], timeout: 30.0)
        
    }
    
    func testSendTemplate() {
        
        let expectation = XCTestExpectation(description: "Send a template with mandrill")
        
        let mandrill = try! MandrillSwiftAPI("")
        
        let tm = TemplateRequest.Message(
            html: nil,
            text: nil,
            subject: "Test Mail",
            fromEmail: "test@email.com",
            fromName: "Admin at Email",
            to: [TemplateRequest.Message.To(email: "test@email.com", name: "Andrew")],
            headers: [
                "Reply-To": "test@email.com"
            ],
            important: false,
            trackOpens: true,
            trackClicks: true,
            autoText: true,
            autoHTML: true,
            inlineCss: false,
            urlStripQs: false,
            preserveRecipients: false,
            viewContentLink: false,
            bccAddress: nil,
            trackingDomain: nil,
            signingDomain: nil,
            returnPathDomain: nil,
            merge: nil,
            mergeLanguage: .mailchimp,
            globalMergeVars: nil,
            mergeVars: nil,
            tags: nil,
            subaccount: nil,
            googleAnalyticsCampaign: nil,
            metadata: nil,
            recipientMetadata: nil,
            attachments: nil,
            images: nil
        )
        
        let template = TemplateRequest(
            key: "",
            templateName: "Appointment EOI Request Confirmation",
            templateContent: [],
            message: tm,
            async: true,
            ipPool: nil,
            sendAt: nil
        )
        
        try! mandrill.api.messages.sendTemplate(templateRequest: template) {
            response in
            
            switch response {
            case .success(let res):
                
                expectation.fulfill()
                print(res)
            case .error(let err):
                print(err)
                XCTFail("fail condition")
            }
        }
        
        wait(for: [expectation], timeout: 30.0)
        
    }

    static var allTests = [
        ("testSendTemplate", testSendTemplate),
        ("testListScheduledMessages", testListScheduledMessages)
    ]
}
